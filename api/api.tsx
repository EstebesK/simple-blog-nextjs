import axios from "axios";

const axiosInstance = axios.create({
  baseURL: `https://simple-blog-api.crew.red/`,
});

export const postsApi = {
    getAllPosts() {
        return axiosInstance.get('posts')
            .then(response => {
                return response.data
            })
    },

    createPost(title: string, body: string) {
        return axiosInstance.post('posts', {title, body})
            .then(response => {
                return response.data
            })
    },

}

export const commentsApi = {
    getPostWithComments(postId: number) {
        return axiosInstance.get(`posts/${postId}?_embed=comments`)
            .then(response => {
                return response.data
            })
    },


    postComment(comment: string) {
        return axiosInstance.post('comments', {comment})
        .then(response => {
            return response.data
        })
    }
}
