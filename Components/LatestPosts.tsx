import React from "react";
import { useActions } from "../hooks/useActions";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { Link as MuiLink, Typography } from "@material-ui/core/";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Link from "./../src/Link";
import { PostType } from './../types/posts';

const LatestPosts = () => {
  const { getAllPosts, getPostWithComments } = useActions();
  const { posts } = useTypedSelector((state) => state.posts);

  React.useEffect(() => {
    getAllPosts();
  }, []);

  const readPost = (postId: number) => {
    getPostWithComments(postId);
  };

  return (
    <Grid item xs={7}>
      {posts.map((post: PostType) => (
        <Paper elevation={3} key={post.id}>
          <Grid
            container
            style={{ height: "100px", margin: "20px" }}
            alignItems='center'
          >
            <MuiLink
              component={Link}
              href={`/posts/${encodeURIComponent(post.id)}`}
              onClick={() => readPost(post.id)}
            >
              <Typography variant="h6">{post.title}</Typography>
            </MuiLink>
          </Grid>
        </Paper>
      ))}
    </Grid>
  );
};

export default LatestPosts;
