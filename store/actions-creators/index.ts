import * as PostsActionCreators from '../actions-creators/posts'
import * as PostWithCommentsActionCreators from '../actions-creators/postWithComments'


export default {
    ...PostsActionCreators,
    ...PostWithCommentsActionCreators
}
