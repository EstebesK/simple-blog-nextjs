import { Dispatch } from "redux";
import { commentsApi } from "../../api/api";
import { CommentsType, PostWithCommentsAction, PostWithCommentsActionsTypes, PostWithCommentsState } from './../../types/postWithComments';



export const setPostWithComments = (payload: PostWithCommentsState): PostWithCommentsAction => {
    return {type: PostWithCommentsActionsTypes.SET_POST_WITH_COMMENTS, payload}
}

export const getPostWithComments = (postId: number) => {

    return async (dispatch: Dispatch<PostWithCommentsAction>) => {
        const data = await commentsApi.getPostWithComments(postId)
                dispatch(setPostWithComments(data));
    }
}

export const addCommentary = (payload: CommentsType): PostWithCommentsAction => {
    return {type: PostWithCommentsActionsTypes.ADD_COMMENTARY, payload}
}

export const addCommentaryThunk = (body: string) => {

    return async (dispatch: Dispatch<PostWithCommentsAction>) => {
        const data = await commentsApi.postComment(body)
                dispatch(addCommentary({...data, body}));
    }
}