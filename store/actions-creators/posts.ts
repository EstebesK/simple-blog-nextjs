import router from "next/router";
import { Dispatch } from "redux";
import { postsApi } from "../../api/api";
import { PostsAction, PostsActionsTypes, PostType } from "../../types/posts";

export const setAllPosts = (payload: PostType[]): PostsAction => {
    return {type: PostsActionsTypes.SET_POSTS, payload}
}

export const getAllPosts = () => {

    return async (dispatch: Dispatch<PostsAction>) => {
        const data = await postsApi.getAllPosts()
                dispatch(setAllPosts(data));
    }
}

export const addPost = (payload: PostType): PostsAction => {
    return {type: PostsActionsTypes.ADD_POST, payload}
}

export const addPostThunk = (title: string, body: string) => {
    
    return async (dispatch: Dispatch<PostsAction>) => {

        const data = await postsApi.createPost(title, body)
                dispatch(addPost(data));
                router.push('/')
    }
}