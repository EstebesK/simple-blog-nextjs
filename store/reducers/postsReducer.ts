import {PostsState, PostsActionsTypes, PostsAction} from "../../types/posts";

const initialState: PostsState = {
    posts: []
}

export const postsReducer = (state = initialState, action: PostsAction): PostsState => {
    switch (action.type) {
        case PostsActionsTypes.SET_POSTS:
            return {
                ...state, posts: action.payload
            }
            case PostsActionsTypes.ADD_POST:
            return {
                    ...state, posts: [...state.posts, {...action.payload}]
            }
        default:
            return state
    }
}
