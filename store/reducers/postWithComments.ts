import {
  PostWithCommentsState,
  PostWithCommentsActionsTypes,
  PostWithCommentsAction,
} from "../../types/postWithComments";

const initialState: PostWithCommentsState = {
  id: null,
  title: null,
  body: null,
  comments: [],
};

export const postWithCommentsReducer = (
  state = initialState,
  action: PostWithCommentsAction
): PostWithCommentsState => {
  switch (action.type) {
    case PostWithCommentsActionsTypes.SET_POST_WITH_COMMENTS:
      return {
        ...state,
        ...action.payload,
      };
    case PostWithCommentsActionsTypes.ADD_COMMENTARY:
      return {
        ...state,
        comments: [...state.comments, {...action.payload}]
      };
    default:
      return state;
  }
};
