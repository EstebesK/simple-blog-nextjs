export interface PostWithCommentsState {
  id: number | null;
  title: string | null;
  body: string | null;
  comments: CommentsType[];
}

export interface CommentsType {
  id: number;
  postId: number;
  body: string;
}

interface SetPostWithComments {
  type: PostWithCommentsActionsTypes.SET_POST_WITH_COMMENTS,
  payload: PostWithCommentsState
}

interface AddCommentary {
  type: PostWithCommentsActionsTypes.ADD_COMMENTARY,
  payload: CommentsType
}

export enum PostWithCommentsActionsTypes {
  SET_POST_WITH_COMMENTS = "SET_POST_WITH_COMMENTS",
  ADD_COMMENTARY = 'ADD_COMMENTARY'
}


export type PostWithCommentsAction = SetPostWithComments | AddCommentary
