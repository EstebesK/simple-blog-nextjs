export interface PostsState {
  posts: PostType[]
}

export interface PostType {
  id: number;
  title: string;
  body: string;
}

export enum PostsActionsTypes {
  SET_POSTS = "SET_POSTS",
  ADD_POST = "ADD_POST",
  DELETE_POST = "DELETE_POST",
  UPLOADING_POST = "UPLOADING_POST"
}

export interface SetPosts {
  type: PostsActionsTypes.SET_POSTS;
  payload: PostType[];
}

interface AddPost {
  type: PostsActionsTypes.ADD_POST,
  payload: PostType
}

export type PostsAction = SetPosts | AddPost ;
