import React from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Link from "./../../src/Link";
import { useInput } from "../../hooks/useInput";
import { useActions } from "../../hooks/useActions";

const NewPost = () => {
  const { addPostThunk } = useActions();

  const title = useInput("");
  const body = useInput("");

  const submit = () => {
    addPostThunk(title.value, body.value);
  };

  return (
    <Grid
      container
      direction="column"
      style={{ paddingLeft: "50px" }}
      spacing={2}
    >
      <Grid item>
        <Typography variant="h3">Simple Blog</Typography>
      </Grid>
      <Grid item>
        <Button color="primary" variant="contained" component={Link} href="/">
          Back to Main Page
        </Button>
      </Grid>
      <Grid item>
        <Grid
          container
          style={{ width: "600px" }}
          direction="column"
          spacing={2}
        >
          <Grid item xs={6}>
            <TextField {...title} placeholder="title" fullWidth />
          </Grid>
          <Grid item xs={6}>
            <TextField
              {...body}
              placeholder="body"
              multiline
              maxRows={6}
              fullWidth
            />
          </Grid>
          <Grid item>
            <Button
              color="primary"
              variant="contained"
              onClick={() => submit()}
            >
              Submit
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default NewPost;
