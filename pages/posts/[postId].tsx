import React from "react";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Link from "../../src/Link";
import TextField from "@material-ui/core/TextField";
import { useActions } from "../../hooks/useActions";
import { useInput } from "../../hooks/useInput";
import { CommentsType } from "../../types/postWithComments";

const postId = () => {
  
  const { title, body, comments } = useTypedSelector(
    (state) => state.postWithComments
  );

  const { addCommentaryThunk } = useActions();

  const commentary = useInput("");

  const submit = () => {
    addCommentaryThunk(commentary.value);
  };


  return (
    <Grid
      container
      direction="column"
      style={{ paddingLeft: "50px" }}
      spacing={2}
    >
      <Grid item>
        <Typography variant="h3">Simple Blog</Typography>
      </Grid>
      <Grid item>
        <Button color="primary" variant="contained" component={Link} href="/">
          Back to Main Page
        </Button>
      </Grid>
      <Grid item>
        <Typography variant="h3">{title}</Typography>
      </Grid>
      <Grid item>
        <Typography variant="body1">{body}</Typography>
      </Grid>
      <Grid item style={{ paddingTop: "10px" }}>
        <Typography variant="h4">Commentaries:</Typography>
        <Grid />
        <Grid item>
          <TextField {...commentary} placeholder="Write a comment" />
        </Grid>
        <Grid item>
            <Button color='primary' variant='contained' onClick={() => submit()}>Submit</Button>
        </Grid>
        <Grid item xs={7}>
          {comments.map((comment: CommentsType) => (
            <Paper elevation={3} key={comment.id}>
              <Grid container direction="column" style={{margin: '20px'}}>
                <Grid item style={{ padding: "20px"}}>
                  <Typography>{comment.body}</Typography>
                </Grid>
              </Grid>
            </Paper>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default postId;
