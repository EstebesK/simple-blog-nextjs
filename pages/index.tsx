import React from "react";
import Typography from "@material-ui/core/Typography";
import LatestPosts from "../Components/LatestPosts";
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';
import Link from './../src/Link';

export default function Index() {
  return (
    <Grid container direction='column' style={{paddingLeft: '50px'}} spacing={2}>
      <Grid item>
        <Typography variant="h3">Simple Blog</Typography>
      </Grid>
      <Grid item>
        <Button color='primary' variant='contained' component={Link} href='/posts/new'>Write new Post</Button>
      </Grid>
      <Grid item>
        <LatestPosts />
      </Grid>
    </Grid>
  );
}
